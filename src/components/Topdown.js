import React from 'react';

class Select extends React.Component {
  
  constructor(props) {
    super();
    
    this.handleOptions = this.handleOptions.bind(this);
  }
  
  handleOptions(objProps, objOneProp) {
    let options = [];
    for(let i=0; i<objOneProp.length; i++){
       options.push(<option key={i} value={objProps[objOneProp[i]]}>{objProps[objOneProp[i]]}
      </option>);
      }
    return options;
  }
  
  render() {
    let objProps = this.props.fValue;
    let objValues = Object.values(objProps);

//kako se izvlace podaci iz objecta, menjaj samo 1 i 0 pise ispod
    let objOneProp = Object.getOwnPropertyNames(objProps);
    let options = this.handleOptions(objProps, objOneProp);
    
    return (
      <div className="col">
      <select className="dropDown">
      {  
       options 
      }
      </select>
      </div>
    );    
  }
}

export default Select;