import React, { Component } from 'react';
import {
  BrowserRouter as Router, Route, Link
} from 'react-router-dom';
import './App.css';

import Users from './Users';
import Uploads from './Uploads';
import Cars from './Cars';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App container">
          <div className="row menu">
              <div className="col-sm-2"></div>
              <div className="col-sm-3"><Link to="/">Users</Link></div>
              <div className="col-sm-2"><Link to="/Uploads">Uploads</Link></div>
              <div className="col-sm-3"><Link to="/Cars">Cars</Link></div>
              <div className="col-sm-2"></div>
          </div>
      
          <div className="page">
            <Route exact path="/" component={Users} />
            <Route path="/Uploads" component={Uploads} />
            <Route path="/Cars" component={Cars} />
          </div>
      
        </div>
      </Router>
    );
  }
}

export default App;
