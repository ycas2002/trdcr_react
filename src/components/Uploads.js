import React from 'react';
import TableHead from './Tablehead';
import Field from './Field';
import Select from './Topdown';

class Uploads extends React.Component {
  
  constructor(props){
    super(props);
    
    this.state = {
      uploads: [],
      sortSel: (a,b) => {
          return (a['id'] > b['id']) ;
          },
      sortOrd: 'asc',
      sname: upload => upload.username.toLowerCase().includes(''),
      username: '',
      filename: '',
      datetime: [],
    }
    
    this.handleFilteredData = this.handleFilteredData.bind(this);
    this.sortBy = this.sortBy.bind(this);
  }
  
  getHeader() {
    let columnsIn = this.state.uploads[0];
    let colName = [];
    
    for(let keys in columnsIn) {
      keys = keys.replace('_', ' ');
      keys = keys.charAt(0).toUpperCase() + keys.slice(1);
      colName.push(keys);
    }
    console.log(colName.length);
    return colName;
  }
  
  sortBy(newSort) {
    let selKey = newSort;
    let sortO = this.state.sortOrd;
    let newOrder = '';
    
    if(sortO === 'asc') {
      newOrder = (a,b) => (a[selKey] > b[selKey]);
      sortO = 'desc';
    } else {
      newOrder = (a,b) => (b[selKey] > a[selKey]);
      sortO = 'asc';
    }
    this.setState({
      sortSel: newOrder,
      sortOrd: sortO
      });
  }

  handleFilteredData(event){ 
    if(event.target.id === 'searchusername') {
          this.setState({username: event.target.value});
          this.setState({sname: upload => upload.username.toLowerCase().includes(this.state.username.toLowerCase())});
      } 
      else if(event.target.id === 'searchfilename') {
          this.setState({filename: event.target.value});
          this.setState({sname: upload => upload.filename.toLowerCase().includes(this.state.filename.toLowerCase())});
      }
      else if(event.target.id === 'searchdate') {    
// Vidi sta ces sa ovim
          this.setState({city: event.target.value});
          this.setState({sname: upload => upload.datetime.toLowerCase().includes(this.state.datetime.toLowerCase())});
      }
  }
  
  componentDidMount() {
    fetch('http://localhost:3001/table/Uploads')
      .then(results => results.json())
      .then(uploads => this.setState({uploads}))
  }
  render(){
    let colName = this.getHeader();
    
    let uploadList = this.state.uploads.sort(this.state.sortSel).filter(this.state.sname).map(upload =>
          <div className="row no-gutters" key={upload.id}>

            <Field fValue={upload.id} />
            <Field fValue={upload.username} key={upload.username} />
            <Field fValue={upload.filename} key={upload.filename} />
            <Select fValue={upload.datetime} key={upload.datetime} />

          </div>
    );
    
    return(
      <div className="uploads">
      <div className="row sectiontitle">Uploads</div>
        <TableHead colName={colName} newSort={this.sortBy}/>
      
        <div className='row searchbox'>
          <div className="col-sm-1"></div>
          <div className="col">
            <input className="textinput" type="text" id='searchusername' onChange={this.handleFilteredData} value={this.state.username} placeholder='search by username'/>
          </div>
          <div className="col">
            <input className="textinput" type="text" id='searchfilename' onChange={this.handleFilteredData} value={this.state.filename} placeholder='search by filename'/>
          </div>
          <div className="col">
            <input className="textinput" type="text" id='searchdate' onChange={this.handleFilteredData} value={this.state.datetime} placeholder='search by datetime'/>
          </div>
        </div>
        <div className="results">
          {uploadList}
        </div>
      </div>
    );
  }
}
export default Uploads;