import React from 'react';
import TableHead from './Tablehead';
import Field from './Field';

class Users extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      users: [],
      header: [],
      sortSel: (a,b) => {
          return (a['id'] > b['id']) ;
          },
      sortOrd: 'asc',
      sname: user => user.first_name.toLowerCase().includes(''),
      firstname: '',
      lastname: '',
      email: ''
    }
    this.getHeader = this.getHeader.bind(this);
    this.sortBy = this.sortBy.bind(this);
    this.handleFilteredData = this.handleFilteredData.bind(this);
  }
  handleFilteredData(event){ 
    if(event.target.id === 'searchfirst') {
          this.setState({firstname: event.target.value});
          this.setState({sname: user => user.first_name.toLowerCase().includes(this.state.firstname.toLowerCase())});
      } 
      else if(event.target.id === 'searchlast') {
          this.setState({lastname: event.target.value});
          this.setState({sname: user => user.last_name.toLowerCase().includes(this.state.lastname.toLowerCase())});
      }
      else if(event.target.id === 'searchmail') {
          this.setState({mail: event.target.value});
          this.setState({sname: user => user.email.includes(this.state.mail)});
      }
  }
  
  sortBy(newSort) {
    let selKey = newSort;
    let sortO = this.state.sortOrd;
    let newOrder = '';
    
    if(sortO === 'asc') {
      newOrder = (a,b) => (a[selKey] > b[selKey]);
      sortO = 'desc';
    } else {
      newOrder = (a,b) => (b[selKey] > a[selKey]);
      sortO = 'asc';
    }
    this.setState({
      sortSel: newOrder,
      sortOrd: sortO
      });
  }

  componentDidMount() {
    fetch('http://localhost:3001/table/Users')
      .then(results => results.json())
      .then(users => this.setState({users}))
  }
  
  getHeader() {
    let columnsIn = this.state.users[0];
    let colName = [];
    
    for(let keys in columnsIn) {
      keys = keys.replace('_', ' ');
      keys = keys.charAt(0).toUpperCase() + keys.slice(1);
      colName.push(keys);
      console.log(keys);
    }
    console.log(colName.length);
    return colName;
  }
  
  render(){
    let colName = this.getHeader();
    let userList = this.state.users.sort(this.state.sortSel).filter(this.state.sname).map(user =>
          <div className="row no-gutters" key={user.id}>
                                                                  
            <Field fValue={user.id} />
            <Field fValue={user.first_name} />
            <Field fValue={user.last_name} />
            <Field fValue={user.email} />
                                                                 
          </div>
          );
    return(
      <div className="users">
      <div className="row sectiontitle">Users</div>
        <TableHead colName={colName} newSort={this.sortBy}/>
      
        <div className='row searchbox'>
          <div className="col-sm-1"></div>
          <div className="col">
            <input className="textinput" type="text" id='searchfirst' onChange={this.handleFilteredData} value={this.state.firstname} placeholder='search by first name'/>
          </div>
          <div className="col">
            <input className="textinput" type="text" id='searchlast' onChange={this.handleFilteredData} value={this.state.lastname} placeholder='search by last name'/>
          </div>
          <div className="col">
            <input className="textinput" type="text" id='searchmail' onChange={this.handleFilteredData} value={this.state.mail} placeholder='search by e-mail'/>
          </div>
        </div>
        <div className="results">
          {userList}
        </div>
      </div>
    );
  }  
}
export default Users;