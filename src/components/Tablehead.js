import React from 'react';

class TableHead extends React.Component {
  constructor(props) {
    super(props);
    
    this.handleSort = this.handleSort.bind(this);
  }
  
  handleSort(event) {
    let selKey = event.target.id;
    selKey = selKey.toLowerCase();
    selKey = selKey.replace(' ', '_');
    this.props.newSort(selKey);
  }
  
  render() {
    let colName = this.props.colName;
    
    let classNVal = '';
    
    let row = colName.map(key => {
        if(key == 'id' || key == 'Id') {
          return <div className='col-sm-1' id={key.replace(' ', '_').toLowerCase()} key={key} onClick={this.handleSort}>{key}</div>;
        } else {
          return <div className='col' id={key.replace(' ', '_').toLowerCase()} key={key} onClick={this.handleSort}>{key}</div>;
        }
    });
    
    return(
    <div className="row no-gutters tablehead">
      {row}
    </div>
    );
  }
}

export default TableHead;