import React from 'react';
import TableHead from './Tablehead';
import Field from './Field';

class Cars extends React.Component {
  
  constructor(props){
    super(props);
    
    this.state = {
      cars: [],
      sortSel: (a,b) => {
          return (a['id'] > b['id']) ;
          },
      sortOrd: 'asc',
      sname: car => car.car.toLowerCase().includes(''),
      carmodel: '',
      payment_method: '',
      city: '',
    }
    
    this.getHeader = this.getHeader.bind(this);
//    this.getList = this.getList.bind(this);
    this.handleFilteredData = this.handleFilteredData.bind(this);
    this.sortBy = this.sortBy.bind(this);
  }
  
  getHeader() {
    let columnsIn = this.state.cars[0];
    let colName = [];
    
    for(let keys in columnsIn) {
      keys = keys.replace('_', ' ');
      keys = keys.charAt(0).toUpperCase() + keys.slice(1);
      colName.push(keys);
    }
    console.log(colName.length);
    return colName;
  }
  
  sortBy(newSort) {
    let selKey = newSort;
    let sortO = this.state.sortOrd;
    let newOrder = '';
    
    if(sortO === 'asc') {
      newOrder = (a,b) => (a[selKey] > b[selKey]);
      sortO = 'desc';
    } else {
      newOrder = (a,b) => (b[selKey] > a[selKey]);
      sortO = 'asc';
    }
    this.setState({
      sortSel: newOrder,
      sortOrd: sortO
      });
  }
  
  handleFilteredData(event){ 
    if(event.target.id === 'searchcar') {
          this.setState({carmodel: event.target.value});
          this.setState({sname: car => car.car.toLowerCase().includes(this.state.carmodel.toLowerCase())});
      } 
      else if(event.target.id === 'searchpayment') {
          this.setState({payment_method: event.target.value});
          this.setState({sname: car => car.payment_method.toLowerCase().includes(this.state.payment_method.toLowerCase())});
      }
      else if(event.target.id === 'searchcity') {
          this.setState({city: event.target.value});
          this.setState({sname: car => car.city.toLowerCase().includes(this.state.city.toLowerCase())});
      }
  }
  
  componentDidMount() {
    fetch('http://localhost:3001/table/Cars')
      .then(results => results.json())
      .then(cars => this.setState({cars}))
  }
  
  render(){
    let colName = this.getHeader();
    let carList = this.state.cars.sort(this.state.sortSel).filter(this.state.sname).map(car =>
      <div className="row no-gutters" key={car.id}>

        <Field fValue={car.id} />
        <Field fValue={car.car} />
        <Field fValue={car.payment_method} />
        <Field fValue={car.city} />
        <Field fValue={car.currency} />

      </div>
      );
    
    return (
      <div className="cars">
      <div className="row sectiontitle">Some thing to do with the cars</div>
        <TableHead colName={colName} newSort={this.sortBy}/>
      
        <div className='row searchbox'>
            <div className="col-sm-1"></div>
            <div className="col">
              <input className="textinput" type="text" id='searchcar' onChange={this.handleFilteredData} value={this.state.carmodel} placeholder='search by car model'/>
            </div>
            <div className="col">
              <input className="textinput" type="text" id='searchpayment' onChange={this.handleFilteredData} value={this.state.payment_method} placeholder='search by payment method'/>
            </div>
            <div className="col">
            </div>
            <div className="col">
              <input className="textinput" type="text" id='searchcity' onChange={this.handleFilteredData} value={this.state.city} placeholder='search by city'/>
            </div>
          </div>
          <div className="results"> 
            {carList}
          </div>
      </div>
    );
  }
}
export default Cars;