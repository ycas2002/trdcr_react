import React from 'react';

class Field extends React.Component {

  render() {
    let styleDiv = {
      textAlign: 'right',
    }
    let styleVal = {};
    let classNVal = '';
    let fValue = this.props.fValue;
    
    let regExpUrl = /http\:\/\/www\.mydomain\.com\/version\.php/i;
    let regExpMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if(Number.isInteger(fValue)){
      styleVal = styleDiv;
      classNVal = 'col-sm-1';       
    } else if(isNaN(fValue) == false && fValue%1 !== 0) {
      fValue = '$' + fValue;
      classNVal = 'col';
    } else if(fValue.match(regExpUrl)) {
    fValue = <a href={fValue}>{fValue}</a>;
      classNVal = 'col';
    } else if(fValue.match(regExpMail)) {
      fValue = <a href={'mailto:' + fValue}>{fValue}</a>;        
      classNVal = 'col';
    } else {
      classNVal = 'col';
    }
    
    
    return <div className={classNVal}style={styleVal}>{fValue}</div>;
  }
  
}

export default Field;